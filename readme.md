# LyGo Events

![](./icon.png)

Event emitter.

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_events`

## Dependencies

`go get -u bitbucket.org/lygo/lygo_commons`

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.10
git push origin v0.1.10
```