package test

import (
	"bitbucket.org/lygo/lygo_events"
	"fmt"
	"testing"
	"time"
)

var count int

func TestTick(t *testing.T) {

	i := 0
	emitter := lygo_events.NewEmitter()
	emitter.Tick(3*time.Second, func(ticker *lygo_events.EventTicker) {
		i++
		switch i {
		case 3:
			ticker.Stop()
		}
		fmt.Println("emitter.Tick")
	}).Join()

}

func TestTicker(t *testing.T) {

	ticker := lygo_events.NewEventTicker(1*time.Second, func(et *lygo_events.EventTicker) {
		fmt.Println("tick...")
	})
	ticker.Stop()
	ticker.Start()
	fmt.Println("START")
	time.Sleep(3*time.Second)
	fmt.Println("PAUSE")
	ticker.Pause()
	time.Sleep(3*time.Second)
	fmt.Println("RESUME")
	ticker.Resume()
	time.Sleep(3*time.Second)
	fmt.Println("STOP")
	ticker.Stop()
	time.Sleep(3*time.Second)
	fmt.Println("START")
	ticker.Start()
	time.Sleep(3*time.Second)
	fmt.Println("QUIT")
	ticker.Stop()
}

func TestEvents(t *testing.T) {
	emitter := lygo_events.NewEmitter()
	emitter.On("my-event", func(event *lygo_events.Event) {
		fmt.Println("listener 1:", event.Name, event.Arguments)
	})
	emitter.On("my-event", func(event *lygo_events.Event) {
		fmt.Println("listener 2:", event.Name, event.Arguments)
	})
	emitter.On("my-event", listener3)
	emitter.Emit("no listener")
	emitter.Emit("my-event", "arg1", 2, 3, "arg4")

	// ASYNC
	emitter.EmitAsync("my-event", "this", "is", "async", "event")

	time.Sleep(1*time.Second)

	emitter.Off("my-event", listener3)
	emitter.Emit("my-event", "SHOULD NOT BE HANDLED FROM LISTENER 3")
	emitter.Off("my-event")
	emitter.Emit("my-event", "SHOULD NOT HANDLE THIS")

	emitter.Clear()
	emitter.Emit("my-event", "SHOULD NOT HANDLE THIS")

}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func listener3(event *lygo_events.Event) {
	fmt.Println("listener 3:", event.Name, event.Arguments)
}

func callback(w *lygo_events.EventTicker) {
	fmt.Println("callback")
	count++
	if count == 3 {
		// stop the ticker permanently
		w.Stop()
		w.Start() // do not start after was stopped
		fmt.Println("STOPPED")
		return
	} else if count == 1 {
		w.Pause()
		fmt.Println("PAUSED")
		time.Sleep(3 * time.Second)
		w.Resume()
		fmt.Println("RESUMED")
	}
}
